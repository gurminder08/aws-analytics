package com.amazonaws.lambda.utils;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClientBuilder;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.util.Base64;

public class ConnectionUtils {

	public static final String MySQL_DB_DRIVER = "com.mysql.cj.jdbc.Driver";

	public static final String MySQL_DB_CONNECTION = System.getenv("MySQL_DB_CONNECTION");
	public static final String MySQL_DB_USER = System.getenv("MySQL_DB_USER");
	public static final String MySQL_DB_PASSWORD = System.getenv("MySQL_DB_PASSWORD");

	public static DataSource getMySQLDBDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(MySQL_DB_DRIVER);
		dataSource.setUrl(MySQL_DB_CONNECTION);
		dataSource.setUsername(MySQL_DB_USER);
		dataSource.setPassword(MySQL_DB_PASSWORD);
		return dataSource;
	}

	private static String decryptConnectionKey() {

		System.out.println("Decrypting Connection key");
		byte[] encryptedKey = Base64.decode(System.getenv("MySQL_DB_CONNECTION"));

		AWSKMS client = AWSKMSClientBuilder.standard().withRegion(Regions.CA_CENTRAL_1).build();

		DecryptRequest request = new DecryptRequest().withCiphertextBlob(ByteBuffer.wrap(encryptedKey));

		ByteBuffer plainTextKey = client.decrypt(request).getPlaintext();
		return new String(plainTextKey.array(), Charset.forName("UTF-8"));
	}

	private static String decryptPasswordKey() {

		System.out.println("Decrypting Password key");
		byte[] encryptedKey = Base64.decode(System.getenv("MySQL_DB_PASSWORD"));

		AWSKMS client = AWSKMSClientBuilder.standard().withRegion(Regions.CA_CENTRAL_1).build();

		DecryptRequest request = new DecryptRequest().withCiphertextBlob(ByteBuffer.wrap(encryptedKey));

		ByteBuffer plainTextKey = client.decrypt(request).getPlaintext();
		return new String(plainTextKey.array(), Charset.forName("UTF-8"));
	}

	public static Connection getMySQLDBConnection() {
		Connection connection = null;
		try {
			Class.forName(MySQL_DB_DRIVER);
			connection = DriverManager.getConnection(MySQL_DB_CONNECTION, MySQL_DB_USER, MySQL_DB_PASSWORD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// System.out.println("Connection" + connection);
		return connection;
	}

	public static void closeMySQLConnection(Connection dbConnection, Statement stmt, ResultSet rs) throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (stmt != null) {
			stmt.close();
		}
		if (dbConnection != null) {
			dbConnection.close();
		}
	}
}
