package com.amazonaws.lambda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.lambda.constants.Constants;
import com.amazonaws.lambda.dao.AnalyticsDAO;
import com.amazonaws.lambda.model.ParticipantMeasureScores;
import com.amazonaws.lambda.utils.ConnectionUtils;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LambdaFunctionHandler implements RequestHandler<Object, String> {

	private static Map<String, Object> mappedInput;
	private static Integer surveyInst;
	// private static ClientConfiguration config = new ClientConfiguration();
	private static AmazonSQS sqs;
	private static String myQueueUrl;

	@Override
	public String handleRequest(Object input, Context context) {

		long start = System.currentTimeMillis();
		ClientConfiguration config = new ClientConfiguration();
		config.setProtocol(Protocol.HTTP);
		config.setConnectionTimeout(250000);
		config.setClientExecutionTimeout(250000);

		sqs = AmazonSQSClientBuilder.standard().withRegion(Regions.CA_CENTRAL_1).withClientConfiguration(config)
				.build();
		myQueueUrl = sqs.getQueueUrl(Constants.QUEUE_NAME).getQueueUrl();
		context.getLogger().log("Input: " + input);
		if (input instanceof Map) {
			mappedInput = (Map<String, Object>) input;
		}
		AnalyticsDAO job = new AnalyticsDAO();
		String surveyInstance = mappedInput.get("surveyInstance").toString();
		surveyInst = Integer.valueOf(Integer.parseInt(surveyInstance.trim()));
		List<ParticipantMeasureScores> surveyInstanceRow;
		try {
			surveyInstanceRow = job.getParticipantMeasureDetails(surveyInstance);
			System.out.println("Rows fetched, " + surveyInstanceRow.size());
			System.out.println("Rem-time after fetch" + context.getRemainingTimeInMillis());
			sendMessagesByQueueUrl(surveyInstanceRow,myQueueUrl);
			System.out.println("Rem time after computation" + context.getRemainingTimeInMillis());
			// System.out.println(sqs.listQueues());

			// System.out.println(sqs.listQueues());
			// this.sendMessagesByQueueUrl(surveyInstanceRow, myQueueUrl);
			// job.printList(surveyInstanceRow);

			// List all queues.
			// System.out.println("Listing all queues in your account.\n");
			// for (final String queueUrl : sqs.listQueues().getQueueUrls()) {
			// System.out.println(" QueueUrl: " + queueUrl);
			// }

			// System.out.println();
			// Send a message.

			// List<Message> messageList = this.getQueueMessageByQueueUrl(myQueueUrl);
			// for (Message message : messageList) {
			// System.out.println("Received message: {}, " + message.toString());
			// }
			// this.deleteMessagesByQueueUrl(messageList, myQueueUrl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// System.out.println("Sending messages to MyQueue.\n");
		//
		// surveyInstanceRow.forEach((row) -> {
		// sqs.sendMessage(new SendMessageRequest(myQueueUrl, ""+row));
		// }
		// );
		//
		// // Receive messages.
		// System.out.println("Receiving messages from MyQueue.\n");
		// final ReceiveMessageRequest receiveMessageRequest = new
		// ReceiveMessageRequest(myQueueUrl);
		//
		// final List<Message> messages =
		// sqs.receiveMessage(receiveMessageRequest).getMessages();
		//
		// for (final Message message : messages) {
		// System.out.println("Message");
		// System.out.println(" MessageId: " + message.getMessageId());
		// System.out.println(" ReceiptHandle: " + message.getReceiptHandle());
		// System.out.println(" MD5OfBody: " + message.getMD5OfBody());
		// System.out.println(" Body: " + message.getBody());
		// for (final Entry<String, String> entry : message.getAttributes().entrySet())
		// {
		// System.out.println("Attribute");
		// System.out.println(" Name: " + entry.getKey());
		// System.out.println(" Value: " + entry.getValue());
		// }
		// }
		//
		// System.out.println();
		//
		// // Delete the message.
		// System.out.println("Deleting a message.\n");
		// final String messageReceiptHandle = messages.get(0).getReceiptHandle();
		// sqs.deleteMessage(new DeleteMessageRequest(myQueueUrl,
		// messageReceiptHandle));
		finally {
		}
		long totalTime = System.currentTimeMillis() - start;
		System.out.println("Total time: " + totalTime);
		return "Hello from lambda, Connection, " + ConnectionUtils.getMySQLDBConnection() + " totalTime, " + totalTime;
	}

	private void sendMessagesByQueueUrl(List<ParticipantMeasureScores> surveyInstanceRow,String queueUrl) throws JsonProcessingException {

		Map<Integer,Float> measureList = new HashMap<Integer,Float>();
		ObjectMapper mapper = new ObjectMapper();
		String jsonMeasure;
		String jsonFinal;
		Integer participantId;
		Integer nextparticipantId;
		Float score;
		Integer genderId;
		Integer gradeId;
		Integer sesId;
		
		for (int i = 0; i < surveyInstanceRow.size(); i++) {

			participantId = surveyInstanceRow.get(i).getSurveyParticipantId();
			nextparticipantId =(i<surveyInstanceRow.size()-1)?surveyInstanceRow.get(i+1).getSurveyParticipantId():0;
			if(participantId.compareTo(nextparticipantId)==0) {
				measureList.put(surveyInstanceRow.get(i).getMeasureId(),surveyInstanceRow.get(i).getScore());
			}
			else if(participantId.compareTo(nextparticipantId)!=0) {
				measureList.put(surveyInstanceRow.get(i).getMeasureId(),surveyInstanceRow.get(i).getScore()); // adding the last measureId of Participant
				genderId = surveyInstanceRow.get(i).getGenderId(); // Computing genderId,gradeId,sesId only for the last row of a particular participant
				gradeId = surveyInstanceRow.get(i).getGradeId();
				sesId = surveyInstanceRow.get(i).getSesId();
				jsonMeasure= mapper.writerWithDefaultPrettyPrinter().writeValueAsString(measureList);
				jsonFinal = constructJson(participantId,jsonMeasure,genderId,gradeId,sesId);
				sqs.sendMessage(new SendMessageRequest(queueUrl, jsonFinal));
				measureList = nextparticipantId!=null?new HashMap<Integer,Float>():measureList;
			}		
		}	
	}
	
	private String constructJson(Integer participantId,String jsonMeasure,Integer genderId, Integer gradeId, Integer sesId) {
		String jsonFinal = "{ \"surveyInstanceId\" : " + surveyInst + " , \"participantId\" : " + participantId
				+ " , \"measureList\" : " + jsonMeasure + ", \"genderId\": " + genderId + ", \"gradeId\": " + gradeId
				+ ", \"sesId\":" + sesId + " }";
		return jsonFinal;
	}

}
