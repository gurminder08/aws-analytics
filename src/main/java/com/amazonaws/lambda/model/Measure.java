package com.amazonaws.lambda.model;

public class Measure {
	Integer id;
	Integer domainId;
	Integer topicId;
	String name;
	Integer reportable;
	Integer drilldownSuppressed;
	Integer compositeMeasureId;
	Double minutesToComplete;
	Integer minimumAnswersRequired;
	String calcRuleName;
	String aggregateRuleName;
	Double minVal;
	Double maxVal;
	Double cutoff;
	String dispUnit;
	Integer position;
	Integer isNew;

	public Measure() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Measure(Integer id, Integer domainId, Integer topicId, String name, Integer reportable,
			Integer drilldownSuppressed, Integer compositeMeasureId, Double minutesToComplete,
			Integer minimumAnswersRequired, String calcRuleName, String aggregateRuleName, Double minVal, Double maxVal,
			Double cutoff, String dispUnit, Integer position, Integer isNew) {
		super();
		this.id = id;
		this.domainId = domainId;
		this.topicId = topicId;
		this.name = name;
		this.reportable = reportable;
		this.drilldownSuppressed = drilldownSuppressed;
		this.compositeMeasureId = compositeMeasureId;
		this.minutesToComplete = minutesToComplete;
		this.minimumAnswersRequired = minimumAnswersRequired;
		this.calcRuleName = calcRuleName;
		this.aggregateRuleName = aggregateRuleName;
		this.minVal = minVal;
		this.maxVal = maxVal;
		this.cutoff = cutoff;
		this.dispUnit = dispUnit;
		this.position = position;
		this.isNew = isNew;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDomainId() {
		return domainId;
	}

	public void setDomainId(Integer domainId) {
		this.domainId = domainId;
	}

	public Integer getTopicId() {
		return topicId;
	}

	public void setTopicId(Integer topicId) {
		this.topicId = topicId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getReportable() {
		return reportable;
	}

	public void setReportable(Integer reportable) {
		this.reportable = reportable;
	}

	public Integer getDrilldownSuppressed() {
		return drilldownSuppressed;
	}

	public void setDrilldownSuppressed(Integer drilldownSuppressed) {
		this.drilldownSuppressed = drilldownSuppressed;
	}

	public Integer getCompositeMeasureId() {
		return compositeMeasureId;
	}

	public void setCompositeMeasureId(Integer compositeMeasureId) {
		this.compositeMeasureId = compositeMeasureId;
	}

	public Double getMinutesToComplete() {
		return minutesToComplete;
	}

	public void setMinutesToComplete(Double minutesToComplete) {
		this.minutesToComplete = minutesToComplete;
	}

	public Integer getMinimumAnswersRequired() {
		return minimumAnswersRequired;
	}

	public void setMinimumAnswersRequired(Integer minimumAnswersRequired) {
		this.minimumAnswersRequired = minimumAnswersRequired;
	}

	public String getCalcRuleName() {
		return calcRuleName;
	}

	public void setCalcRuleName(String calcRuleName) {
		this.calcRuleName = calcRuleName;
	}

	public String getAggregateRuleName() {
		return aggregateRuleName;
	}

	public void setAggregateRuleName(String aggregateRuleName) {
		this.aggregateRuleName = aggregateRuleName;
	}

	public Double getMinVal() {
		return minVal;
	}

	public void setMinVal(Double minVal) {
		this.minVal = minVal;
	}

	public Double getMaxVal() {
		return maxVal;
	}

	public void setMaxVal(Double maxVal) {
		this.maxVal = maxVal;
	}

	public Double getCutoff() {
		return cutoff;
	}

	public void setCutoff(Double cutoff) {
		this.cutoff = cutoff;
	}

	public String getDispUnit() {
		return dispUnit;
	}

	public void setDispUnit(String dispUnit) {
		this.dispUnit = dispUnit;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getIsNew() {
		return isNew;
	}

	public void setIsNew(Integer isNew) {
		this.isNew = isNew;
	}

	@Override
	public String toString() {
		return "Measure [id=" + id + ", domainId=" + domainId + ", topicId=" + topicId + ", name=" + name
				+ ", reportable=" + reportable + ", drilldownSuppressed=" + drilldownSuppressed
				+ ", compositeMeasureId=" + compositeMeasureId + ", minutesToComplete=" + minutesToComplete
				+ ", minimumAnswersRequired=" + minimumAnswersRequired + ", calcRuleName=" + calcRuleName
				+ ", aggregateRuleName=" + aggregateRuleName + ", minVal=" + minVal + ", maxVal=" + maxVal + ", cutoff="
				+ cutoff + ", dispUnit=" + dispUnit + ", position=" + position + ", isNew=" + isNew + "]";
	}
}