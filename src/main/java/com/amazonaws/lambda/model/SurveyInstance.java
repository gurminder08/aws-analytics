package com.amazonaws.lambda.model;

public class SurveyInstance {

	Integer id;
	Integer orgId;
	Integer surveyDefinitionId;
	Integer surveyClosedFlag;
	Integer yearDefId;

	public SurveyInstance() {
		super();
	}

	public SurveyInstance(Integer id, Integer orgId, Integer surveyDefinitionId, Integer surveyClosedFlag,
			Integer yearDefId) {
		super();
		this.id = id;
		this.orgId = orgId;
		this.surveyDefinitionId = surveyDefinitionId;
		this.surveyClosedFlag = surveyClosedFlag;
		this.yearDefId = yearDefId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public Integer getSurveyDefinitionId() {
		return surveyDefinitionId;
	}

	public void setSurveyDefinitionId(Integer surveyDefinitionId) {
		this.surveyDefinitionId = surveyDefinitionId;
	}

	public Integer getSurveyClosedFlag() {
		return surveyClosedFlag;
	}

	public void setSurveyClosedFlag(Integer surveyClosedFlag) {
		this.surveyClosedFlag = surveyClosedFlag;
	}

	public Integer getYearDefId() {
		return yearDefId;
	}

	public void setYearDefId(Integer yearDefId) {
		this.yearDefId = yearDefId;
	}

	@Override
	public String toString() {
		return "SurveyInstance [id=" + id + ", orgId=" + orgId + ", surveyDefinitionId=" + surveyDefinitionId
				+ ", surveyClosedFlag=" + surveyClosedFlag + ", yearDefinitionId=" + getYearDefId() + "]";
	}

}
