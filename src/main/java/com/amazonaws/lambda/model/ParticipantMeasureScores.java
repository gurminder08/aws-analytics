package com.amazonaws.lambda.model;

public class ParticipantMeasureScores {


	Integer surveyParticipantId;
	Integer measureId;
	Float score;
	Integer genderId;
	Integer gradeId;
	Integer sesId;

	public ParticipantMeasureScores() {
		
	}
	
	public ParticipantMeasureScores(Integer surveyParticipantId, Integer measureId, Float score, Integer genderId,
			Integer gradeId, Integer sesId) {
		super();
		this.surveyParticipantId = surveyParticipantId;
		this.measureId = measureId;
		this.score = score;
		this.genderId = genderId;
		this.gradeId = gradeId;
		this.sesId = sesId;
	}
	
	public Integer getSurveyParticipantId() {
		return surveyParticipantId;
	}

	public void setSurveyParticipantId(Integer surveyParticipantId) {
		this.surveyParticipantId = surveyParticipantId;
	}

	public Integer getMeasureId() {
		return measureId;
	}

	public void setMeasureId(Integer measureId) {
		this.measureId = measureId;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public Integer getGenderId() {
		return genderId;
	}

	public void setGenderId(Integer genderId) {
		this.genderId = genderId;
	}

	public Integer getGradeId() {
		return gradeId;
	}

	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	public Integer getSesId() {
		return sesId;
	}

	public void setSesId(Integer sesId) {
		this.sesId = sesId;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "ParticipantMeasureScores [surveyParticipantId= "+surveyParticipantId+" measureId= "+measureId+
				" score=" + score +" genderId= "+genderId+" gradeId= "+gradeId+" sesId= "+sesId+"]";
	}

}
