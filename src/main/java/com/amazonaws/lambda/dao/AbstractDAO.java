package com.amazonaws.lambda.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.lambda.constants.Constants;
import com.amazonaws.lambda.utils.ConnectionUtils;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public abstract class AbstractDAO {

	static ClientConfiguration config = new ClientConfiguration();
	private AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.CA_CENTRAL_1).build();

	protected StringBuffer sqlBuf;
	protected BufferedReader bufReader;
	protected JdbcTemplate jdbcTemplate;
	protected String sql;

	public AbstractDAO() {

	}

	public JdbcTemplate getJdbcTemplateDB() {
		setJdbcTemplateDB();
		return new JdbcTemplate(ConnectionUtils.getMySQLDBDataSource());
	}

	public void setJdbcTemplateDB() {
		this.jdbcTemplate = new JdbcTemplate(ConnectionUtils.getMySQLDBDataSource());
	}

	public String getSQLQueryFromPath(String sqlFileName) throws IOException {

		S3Object response = s3.getObject(new GetObjectRequest(Constants.BUCKET_NAME, Constants.PREFIX + sqlFileName));
		/* Reading queries from the S3 bucket */
		return displayTextInputStream(response.getObjectContent());
		/*
		 * Commented out section below shows earlier approach to read queries locally
		 * from lambda function, does not work Just for reference
		 */

		// sqlBuf = new StringBuffer();
		// try {
		// bufReader = new BufferedReader(new FileReader(queryFolderPath +
		// "db.properties"));
		// String line = "";
		// while ((line = bufReader.readLine()) != null) {
		// line = line.trim();
		// line = line + " ";
		// sqlBuf.append(line);
		// }
		// } catch (IOException e) {
		// e.printStackTrace();
		// } finally {
		// try {
		// bufReader.close();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// }
		// return sqlBuf.toString();
	}

	private String displayTextInputStream(InputStream input) throws IOException {
		// Read the text input stream one line at a time and display each line.
		sqlBuf = new StringBuffer();
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		String line = null;
		while ((line = reader.readLine()) != null) {
			line = line.trim();
			line = line + " ";
			sqlBuf.append(line);
		}
		// System.out.println("made string" + sqlBuf);
		return sqlBuf.toString();
	}

	public String substituteParameter(String str, String replaceStr) {
		int position = str.indexOf(Constants.QUESTION_MARK, 0);
		return str.substring(0, position) + replaceStr + str.substring(position + 1);
	}

	public String substituteParameters(String str, String replaceStr) {

		StringBuilder strB = new StringBuilder(str);
		int firstOccurence = strB.indexOf(Constants.QUESTION_MARK);
		strB.deleteCharAt(firstOccurence);
		strB.insert(firstOccurence, replaceStr);
		int lastOccurence = strB.indexOf(Constants.QUESTION_MARK);
		strB.deleteCharAt(lastOccurence);
		strB.insert(lastOccurence, replaceStr);
		String strFinal = strB.toString();
		return strFinal;
	}

	public <T> void printList(List<T> lstT) {
		boolean flag = true;
		for (T t : lstT) {
			if (flag) {
				System.out.println(t.getClass().getSimpleName());
				flag = false;
			}
			System.out.println(t.toString());
		}
	}
}
