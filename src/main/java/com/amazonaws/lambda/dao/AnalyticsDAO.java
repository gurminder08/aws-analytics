package com.amazonaws.lambda.dao;

import java.io.IOException;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.amazonaws.lambda.model.ParticipantMeasureScores;
import com.amazonaws.lambda.model.SurveyInstance;

public class AnalyticsDAO extends AbstractDAO {

	public List<SurveyInstance> getMeasureDetails(String surveyInstance) throws IOException {
		sql = getSQLQueryFromPath("surveyInstanceDetails.sql");
		sql = substituteParameter(sql, surveyInstance);
		try {
			return getJdbcTemplateDB().query(sql, new BeanPropertyRowMapper<SurveyInstance>(SurveyInstance.class));
		} catch (NullPointerException | EmptyResultDataAccessException e) {
			return null;
		} finally {
			// System.out.println("finally");

		}
	}

	public List<ParticipantMeasureScores> getParticipantMeasureDetails(String surveyInstance) throws IOException {
		// TODO Auto-generated method stub
		sql = getSQLQueryFromPath("participantMeasureScoreDetails.sql");
		sql = substituteParameters(sql, surveyInstance);
		try {
			return getJdbcTemplateDB().query(sql,
					new BeanPropertyRowMapper<ParticipantMeasureScores>(ParticipantMeasureScores.class));

		} catch (Exception e) {
			return null;
		} finally {
			// System.out.println("finally");

		}
	}
}
