
package com.amazonaws.lambda;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;

public class Invoker {

	// public static final String MySQL_DB_CONNECTION =
	// "jdbc:mysql://readdbinstance.chhtkckrgigt.ca-central-1.rds.amazonaws.com:3306/ourschool_analytics";
	private static final String regionName = "ca-central-1";
	private static final String functionName = "workerLambda";
	private static AWSLambda lambdaClient;
	static ClientConfiguration config = new ClientConfiguration();

	public static void main(String args[]) {

		String[] surveyInstances = { "97829" };

		Invoker invoke = new Invoker();
		for (int i = 0; i < surveyInstances.length; i++) {
			invoke.invokeLambda(surveyInstances[i]);

		}

		// AnalyticsDAO job = new AnalyticsDAO();
		//
		// for(int i=0;i<surveyInstances.length;i++) {
		//
		// List<ParticipantMeasureScores>
		// surveyInstanceRow=job.getParticipantMeasureDetails(surveyInstances[i]);
		// job.printList(surveyInstanceRow);
		//
		// }

	}

	private static void invokeLambda(String surveyInstance) {

		try {

			config.setSocketTimeout(250000);
			config.setClientExecutionTimeout(250000);
			// lambdaClient = AWSLambdaAsyncClientBuilder.standard()
			// .withCredentials(new
			// AWSStaticCredentialsProvider(credentials)).withRegion(regionName)
			// .withClientConfiguration(config).build();
			lambdaClient = AWSLambdaClientBuilder.standard().withRegion(Regions.CA_CENTRAL_1)
					.withClientConfiguration(config).build();

			String json = "{ \"surveyInstance\" : \" " + surveyInstance + " \" }";
			InvokeRequest invokeRequest = new InvokeRequest().withFunctionName(functionName)
					.withPayload(ByteBuffer.wrap(json.getBytes()));
			invokeRequest.setFunctionName(functionName);
			invokeRequest.setPayload(json);
			System.out.println(
					byteBufferToString(lambdaClient.invoke(invokeRequest).getPayload(), Charset.forName("UTF-8")));
			// Future<InvokeResult> future_res = lambdaClient.invokeAsync(invokeRequest, new
			// AsyncLambdaHandler());
			// System.out.print("Waiting for async callback "+i);
			// while (!future_res.isDone() && !future_res.isCancelled()) {
			// System.out.println("Performing Other tasks for "+i);
			// try {
			// Thread.sleep(1000);
			// }
			// catch (InterruptedException e) {
			// System.err.println("Thread.sleep() was interrupted! for "+i);
			// System.exit(0);
			// }
			// System.out.print(".");
			// }
		}

		catch (Exception e) {

			System.out.println(e.getMessage());

		}

	}

	private static String byteBufferToString(ByteBuffer buffer, Charset charset) {

		byte[] bytes;

		if (buffer.hasArray()) {
			bytes = buffer.array();
		}

		else {
			bytes = new byte[buffer.remaining()];
			buffer.get(bytes);
		}

		return new String(bytes, charset);
	}

}
