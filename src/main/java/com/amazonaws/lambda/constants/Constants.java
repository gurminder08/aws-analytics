package com.amazonaws.lambda.constants;

public class Constants {

	public static final String QUESTION_MARK = "?";
	public static final String DELIMITER_COMMA = ",";
	public static final String PREFIX= "queries/participantMeasureScoreDetails/";
	public static final String DELIMITER_FORWARD_SLASH="/";
	public static final String BUCKET_NAME="gurminder-analytics";
	public static final String QUEUE_NAME="workerQueue";
}
