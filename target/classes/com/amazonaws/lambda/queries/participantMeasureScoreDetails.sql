Select val.surveyParticipantId, pms.measureId, pms.score,val.genderId,val.gradeId,val.sesId
From tlb.ParticipantMeasureScores as pms 

Inner Join

(Select sp.id as surveyParticipantId, 
MIN(IF(pc.characteristicId = 1, CONVERT(pc.value, UNSIGNED INTEGER), 3)) as genderId,
MIN(IF(pc.characteristicId = 2, CONVERT(pc.value, UNSIGNED INTEGER), 13)) as gradeId,
MIN(IF(pc.characteristicId = 46, CONVERT(pc.value, UNSIGNED INTEGER), 4)) as sesId
From        
tlb.SurveyParticipant as sp 
Inner Join tlb.ParticipantCharacteristic as pc On pc.surveyParticipantId = sp.id
Where sp.surveyInstanceId IN (?) And pc.characteristicId In (1,2,46)
Group By sp.id) as val On val.surveyParticipantId = pms.surveyParticipantId
Where pms.surveyInstanceId IN (?);